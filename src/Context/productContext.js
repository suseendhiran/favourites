import React, { useState, useEffect } from "react";

export const ProductsContext = React.createContext(null);

const ProductsContextProvider = (props) => {
  const list = [];
  const [productsList, setProductsList] = useState([
    {
      id: "p1",
      title: "Red Scarf",
      description: "A pretty red scarf.",
      isFavorite: false,
    },
    {
      id: "p2",
      title: "Blue T-Shirt",
      description: "A pretty blue t-shirt.",
      isFavorite: false,
    },
    {
      id: "p3",
      title: "Green Trousers",
      description: "A pair of lightly green trousers.",
      isFavorite: false,
    },
    {
      id: "p4",
      title: "Orange Hat",
      description: "Street style! An orange hat.",
      isFavorite: false,
    },
  ]);
  const toggleFav = (id) => {
    setProductsList((currProductsList) => {
      const prodIndex = currProductsList.findIndex((p) => p.id === id);
      const newFavStatus = !currProductsList[prodIndex].isFavorite;
      const updatedProducts = [...currProductsList];
      console.log(updatedProducts);
      updatedProducts[prodIndex] = {
        ...currProductsList[prodIndex],
        isFavorite: newFavStatus,
      };

      return updatedProducts;
    });
  };
  useEffect(() => {
    list.push(setProductsList);
    console.log(list);
  }, [productsList]);
  return (
    <ProductsContext.Provider
      value={{ products: productsList, toggleFav: toggleFav }}
    >
      {props.children}
    </ProductsContext.Provider>
  );
};

export default ProductsContextProvider;
